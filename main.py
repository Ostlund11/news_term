# pylint: disable=line-too-long
# pylint: disable=missing-function-docstring
# pylint: disable=bare-except
# pylint: disable=trailing-whitespace
# pylint: disable=missing-module-docstring

#Importing necessarry 
from fastapi import FastAPI
import news_fetch


#Set up the API
app = FastAPI()


#Default endpoint
@app.get("/")
def read_root():
    return {"Hello": "World"}


#Update endpoint. Updates the database with new articles and deletes the old
@app.get("/update")
def update_db():
    print("Updating DB, Please wait...")
    news_fetch.update_db()
    return {"Status": "Complete"}


#Fetch the news articles from the database 
@app.get("/fetch")
def fetch():
    print("Fetching news")
    return news_fetch.fetch()

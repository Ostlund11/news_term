# pylint: disable=line-too-long
# pylint: disable=missing-function-docstring
# pylint: disable=bare-except
# pylint: disable=trailing-whitespace
# pylint: disable=missing-module-docstring

#Import modules
import sys
import requests

try:
    assert sys.argv[2]
except:
    print("Please provide all arguements (Command (update/fetch),Hostname (API))")
    sys.exit()

#Check what the client is trying to fetch and fetch it if it is part of the API
#Update database with new values
if str(sys.argv[1]) == "update":
    URL = ("http://" + str(sys.argv[2]) + "/update")
    try:
        response = requests.get(URL)
        if response.status_code == 200:
            print("Succesfully updated the database")
        else:
            print("Error, could not update database. Error code: " + response.status_code)
    except:
        print("Error, could not update " + URL)

#Fetch news articles
elif str(sys.argv[1]) == "fetch":
    URL = ("http://" + str(sys.argv[2]) + "/fetch")
    try:
        response = requests.get(URL)
        res_json = response.json()
        if response.status_code == 200:
            for art in res_json['Articles']:
                print("\n\nID: " + str(art['id']) + "\nTitle: " + str(art['Title']) + "\nSource: " + str(art['Source']) + "\nDescription: " + str(art['Desc']) + "\nAuthor: " + str(art['Author']) + "\nLink: " + str(art['Link']))
        else:
            print("Error, could not fetch articles. Error code: " + response.status_code)
    except:
        print("Error, could not fetch " + URL)

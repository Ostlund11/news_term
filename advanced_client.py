# pylint: disable=line-too-long
# pylint: disable=missing-function-docstring
# pylint: disable=bare-except
# pylint: disable=trailing-whitespace
# pylint: disable=missing-module-docstring

#Import modules
import sys
import requests
import sqlite3
import os
import time

#Connect to database
Conn = sqlite3.connect('LocalArticleArchive.db')

#Set up Cursor
Cursor = Conn.cursor()


try:
    Cursor.execute('''CREATE TABLE Articles (Id TEXT, Title TEXT, Source TEXT, Desc TEXT, Author TEXT, Link TEXT, Read INT, LocalID INT)''')
except:
    print("Database already exist")



def update():
    Cursor.execute("DROP TABLE Articles")
    Cursor.execute('''CREATE TABLE Articles (Id TEXT, Title TEXT, Source TEXT, Desc TEXT, Author TEXT, Link TEXT, Read INT, LocalID INT)''')

    UPDATE_URL = "http://0.0.0.0/update"
    FETCH_URL = "http://0.0.0.0/fetch"
    requests.get(UPDATE_URL)
    response = requests.get(FETCH_URL)
    responsejson = response.json()
    
    for number, value in enumerate(responsejson['Articles']):
        Cursor.execute("INSERT INTO Articles (Id, Title, Source, Desc, Author, Link, Read, LocalID) VALUES (?, ?, ?, ?, ?, ?, 0, ?)", (str(value['id']), str(value['Title']), str(value['Source']), str(value['Desc']), str(value['Author']), str(value['Link']), number ))
    print("Update Complete!")
    input("\n\n\n\n\n\n\nPress enter to continue")

def read_article(art_id):
    stream = os.popen("clear")
    output = stream.read()
    print(output)
    Cursor.execute("SELECT * FROM Articles WHERE LocalID=?", (art_id,))
    art = Cursor.fetchone()
    print("Title: " + str(art[1]) + "\n\nSource: " + str(art[2]) + "\n\nDescription: " + str(art[3]) + "\n\nAuthor: " + str(art[4]) + "\n\nLink: " + str(art[5]) + "\n\nID: " + str(art[7]))
    Cursor.execute("UPDATE Articles SET Read=1 WHERE LocalID=?", (art_id,))
    input("\n\n\n\n\n\n\nPress enter to continue")


def show_all():
    stream = os.popen("clear")
    output = stream.read()
    print(output)
    Cursor.execute("SELECT * FROM Articles")
    artdb = Cursor.fetchall()
    for art in artdb:
        if art[6] == 0:
            r = "False"
        else:
            r = "True"
        print("\n\n\nID: " + str(art[7]) + "\n\nTitle: " + str(art[1]) + "\n\nRead: " + r)
    input("\n\n\n\n\n\n\nPress enter to continue")

def show_unread():
    stream = os.popen("clear")
    output = stream.read()
    print(output)
    Cursor.execute("SELECT * FROM Articles WHERE Read=0")
    artdb = Cursor.fetchall()
    for art in artdb:
        print("\n\n\nID: " + str(art[7]) + "\n\nTitle: " + str(art[1]))
    input("\n\n\n\n\n\n\nPress enter to continue")

stream = os.popen("clear")
output = stream.read()
print(output)
update()

while True:
    stream = os.popen("clear")
    output = stream.read()
    print(output)
    print("**NEWS TERM**\n\n\n\n\n1. Show all articles\n\n2. Show unread articles\n\n3. Read article\n\n4. Update DB\n\n5. Quit\n\n\n\n\n")
    cmd = input(":")
    try:
        if int(cmd) == 1:
            show_all()
        elif int(cmd) == 2:
            show_unread()
        elif int(cmd) == 3:
            ID = input("ID:")
            read_article(ID)
        elif int(cmd) == 4:
            update()
        elif int(cmd) == 5:
            stream = os.popen("clear")
            output = stream.read()
            print(output)
            sys.exit()
        else:
            print("Error. Try again")
            time.sleep(2)
    except SystemExit:
        sys.exit()
    except:
        print("Error, please enter correct input")
